﻿using Reactive.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Reactive
{
    public class App
    {
        public App()
        {
            App.version = new Framework.Version
            {
                Major = 0,
                Minor = 1,
                Revision = 0,
                Serial = 0,
                Accessibility = Accessibility.Internal
            };
            //System.Windows.Forms.MessageBox.Show(App.Version.ToString());
        }

        public static Framework.Version version;
        public static Framework.Version Version
        {
            get
            {
                return new Framework.Version
                {
                    Major = 0,
                    Minor = 1,
                    Revision = 0,
                    Serial = 0,
                    Accessibility = Accessibility.Internal
                };
            }
        }
    }
}