﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Reactive.Serialization
{
    public class BinarySerializer : Serializer
    {
        private BinaryReader binaryReader;

        private BinaryWriter binaryWriter;

        public BinarySerializer(Stream stream, Serializer.SerializationMode mode) : base(stream, mode)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }
            Serializer.SerializationMode mode2 = this.Mode;
            if (mode2 != Serializer.SerializationMode.Read)
            {
                if (mode2 != Serializer.SerializationMode.Write)
                {
                    throw new ArgumentException("Unsuported serialization mode.", "mode");
                }
                this.binaryWriter = new BinaryWriter(stream);
            }
            else
            {
                this.binaryReader = new BinaryReader(stream);
            }
        }

        public override T SerializeAttribute<T>(string attributeName, T value)
        {
            if (string.IsNullOrEmpty(attributeName))
            {
                throw new ArgumentNullException("attributeName");
            }
            Serializer.SerializationMode mode = this.Mode;
            if (mode != Serializer.SerializationMode.Read)
            {
                if (mode == Serializer.SerializationMode.Write)
                {
                    this.binaryWriter.Write(attributeName);
                    this.binaryWriter.Write(base.ToString<T>(value));
                }
            }
            else
            {
                long position = this.binaryReader.BaseStream.Position;
                string a = this.binaryReader.ReadString();
                if (a == attributeName)
                {
                    string stringValue = this.binaryReader.ReadString();
                    value = base.FromString<T>(stringValue);
                }
                else
                {
                    this.binaryReader.BaseStream.Seek(position, SeekOrigin.Begin);
                }
            }
            return value;
        }

        public override T SerializeElement<T>(string elementName, T value, SerializationOption serializationOptions)
        {
            Serializer.SerializationMode mode = this.Mode;
            if (mode != Serializer.SerializationMode.Read)
            {
                if (mode == Serializer.SerializationMode.Write)
                {
                    long position = this.binaryWriter.BaseStream.Position;
                    this.binaryWriter.Seek(4, SeekOrigin.Current);
                    if (value == null)
                    {
                        this.SerializeAttribute<bool>("Null", true);
                    }
                    else
                    {
                        if ((serializationOptions & SerializationOption.SerializeTypeName) != SerializationOption.Default)
                        {
                            this.SerializeAttribute<string>("Type", value.GetType().AssemblyQualifiedName);
                        }
                        ISerializable serializable = value as ISerializable;
                        if (serializable != null)
                        {
                            VersionProvider versionProvider = base.GetVersionProvider<ISerializable>(serializable);
                            versionProvider = base.SerializeVersionAttribute(versionProvider);
                            serializable.SerializeAttributes(this, versionProvider);
                            serializable.SerializeElements(this, versionProvider);
                            base.ReleaseVersionProvider(versionProvider);
                        }
                        else
                        {
                            this.binaryWriter.Write(base.ToString<T>(value));
                        }
                    }
                    long position2 = this.binaryWriter.BaseStream.Position;
                    uint value2 = (uint)(position2 - position);
                    this.binaryWriter.Seek((int)position, SeekOrigin.Begin);
                    this.binaryWriter.Write(value2);
                    this.binaryWriter.Seek((int)position2, SeekOrigin.Begin);
                }
            }
            else
            {
                long position3 = this.binaryReader.BaseStream.Position;
                uint num = this.binaryReader.ReadUInt32();
                long offset = position3 + (long)((ulong)num);
                bool flag = this.SerializeAttribute<bool>("Null", false);
                if (!flag && value == null && typeof(T) != typeof(string))
                {
                    if ((serializationOptions & SerializationOption.DontInstantiateElements) == SerializationOption.DontInstantiateElements)
                    {
                        this.binaryReader.BaseStream.Seek(offset, SeekOrigin.Begin);
                        return value;
                    }
                    string text = this.SerializeAttribute<string>("Type", string.Empty);
                    try
                    {
                        if (string.IsNullOrEmpty(text))
                        {
                            Type typeFromHandle = typeof(T);
                            value = (T)((object)Activator.CreateInstance(typeFromHandle, true));
                        }
                        else
                        {
                            Type type = Type.GetType(text);
                            value = (T)((object)Activator.CreateInstance(type, true));
                        }
                    }
                    catch (Exception var_7_EE)
                    {
                        this.binaryReader.BaseStream.Seek(offset, SeekOrigin.Begin);
                        return default(T);
                    }
                }
                if (flag)
                {
                    value = default(T);
                }
                else
                {
                    VersionProvider versionProvider2 = null;
                    ISerializable serializable2 = value as ISerializable;
                    if (serializable2 != null)
                    {
                        versionProvider2 = base.GetVersionProvider<ISerializable>(serializable2);
                        versionProvider2 = base.SerializeVersionAttribute(versionProvider2);
                        serializable2.SerializeAttributes(this, versionProvider2);
                    }
                    if (serializable2 != null)
                    {
                        serializable2.SerializeElements(this, versionProvider2);
                        value = (T)((object)serializable2);
                    }
                    else
                    {
                        string stringValue = this.binaryReader.ReadString();
                        value = base.FromString<T>(stringValue);
                    }
                    base.ReleaseVersionProvider(versionProvider2);
                }
            }
            return value;
        }

        public override T[] SerializeElement<T>(string elementName, T[] array, string subElementName, SerializationOption serializationOptions)
        {
            if ((serializationOptions & SerializationOption.AppendContent) == SerializationOption.AppendContent && (serializationOptions & SerializationOption.DontInstantiateElements) == SerializationOption.DontInstantiateElements)
            {
                //throw new Diagnostics.Exception("Incompatible serialization options. AppendContent and DontInstantiateElements can't be activated together.");
            }
            Serializer.SerializationMode mode = this.Mode;
            if (mode != Serializer.SerializationMode.Read)
            {
                if (mode == Serializer.SerializationMode.Write)
                {
                    long position = this.binaryWriter.BaseStream.Position;
                    this.binaryWriter.Seek(4, SeekOrigin.Current);
                    if (array == null)
                    {
                        this.SerializeAttribute<bool>("Null", true);
                    }
                    else
                    {
                        int num = 0;
                        for (int i = 0; i < array.Length; i++)
                        {
                            ISerializationFilter serializationFilter = array[i] as ISerializationFilter;
                            if (serializationFilter == null || !serializationFilter.SkipSerialization)
                            {
                                num++;
                            }
                        }
                        this.SerializeAttribute<int>("Count", num);
                        subElementName = (subElementName ?? string.Format("{0}Item", elementName));
                        int num2 = 0;
                        while (num2 < array.Length && num > 0)
                        {
                            T t = array[num2];
                            ISerializationFilter serializationFilter2 = t as ISerializationFilter;
                            if (serializationFilter2 == null || !serializationFilter2.SkipSerialization)
                            {
                                base.SerializeSubElement<T>(subElementName, t, serializationOptions);
                                num--;
                            }
                            num2++;
                        }
                    }
                    long position2 = this.binaryWriter.BaseStream.Position;
                    uint value = (uint)(position2 - position);
                    this.binaryWriter.Seek((int)position, SeekOrigin.Begin);
                    this.binaryWriter.Write(value);
                    this.binaryWriter.Seek((int)position2, SeekOrigin.Begin);
                }
            }
            else
            {
                int num3 = 0;
                long position3 = this.binaryReader.BaseStream.Position;
                uint num4 = this.binaryReader.ReadUInt32();
                long offset = position3 + (long)((ulong)num4);
                bool flag = this.SerializeAttribute<bool>("Null", false);
                int num5 = this.SerializeAttribute<int>("Count", 0);
                if (flag)
                {
                    array = null;
                }
                else if ((serializationOptions & SerializationOption.AppendContent) == SerializationOption.AppendContent)
                {
                    int num6 = (array == null) ? 0 : array.Length;
                    int newSize = num6 + num5;
                    Array.Resize<T>(ref array, newSize);
                    num3 = num6;
                }
                else if ((serializationOptions & SerializationOption.DontInstantiateElements) != SerializationOption.DontInstantiateElements)
                {
                    if (array == null || array.Length != num5)
                    {
                        array = new T[num5];
                    }
                }
                if (array != null)
                {
                    subElementName = (subElementName ?? string.Format("{0}Item", elementName));
                    int num7 = num3;
                    for (int j = 0; j < num5; j++)
                    {
                        if ((serializationOptions & SerializationOption.DontInstantiateElements) == SerializationOption.DontInstantiateElements)
                        {
                            string text = this.SerializeAttribute<string>("Type", string.Empty);
                            text = ((!string.IsNullOrEmpty(text)) ? text : typeof(T).AssemblyQualifiedName);
                            while (num7 < array.Length && (array[num7] == null || array[num7].GetType().AssemblyQualifiedName != text))
                            {
                                num7++;
                            }
                        }
                        if (num7 < array.Length)
                        {
                            array[num7] = base.SerializeSubElement<T>(subElementName, array[num7], serializationOptions);
                        }
                        else
                        {
                            this.binaryReader.BaseStream.Seek(offset, SeekOrigin.Begin);
                        }
                        num7++;
                    }
                }
            }
            return array;
        }

        public override List<T> SerializeElement<T>(string elementName, List<T> list, string subElementName, SerializationOption serializationOptions)
        {
            if ((serializationOptions & SerializationOption.AppendContent) == SerializationOption.AppendContent && (serializationOptions & SerializationOption.DontInstantiateElements) == SerializationOption.DontInstantiateElements)
            {
                //throw new Diagnostics.Exception("Incompatible serialization options. AppendContent and DontInstantiateElements can't be activated together.");
            }
            Serializer.SerializationMode mode = this.Mode;
            if (mode != Serializer.SerializationMode.Read)
            {
                if (mode == Serializer.SerializationMode.Write)
                {
                    long position = this.binaryWriter.BaseStream.Position;
                    this.binaryWriter.Seek(4, SeekOrigin.Current);
                    if (list == null)
                    {
                        this.SerializeAttribute<bool>("Null", true);
                    }
                    else
                    {
                        int num = 0;
                        for (int i = 0; i < list.Count; i++)
                        {
                            ISerializationFilter serializationFilter = list[i] as ISerializationFilter;
                            if (serializationFilter == null || !serializationFilter.SkipSerialization)
                            {
                                num++;
                            }
                        }
                        this.SerializeAttribute<int>("Count", num);
                        subElementName = (subElementName ?? string.Format("{0}Item", elementName));
                        int num2 = 0;
                        while (num2 < list.Count && num > 0)
                        {
                            T t = list[num2];
                            ISerializationFilter serializationFilter2 = t as ISerializationFilter;
                            if (serializationFilter2 == null || !serializationFilter2.SkipSerialization)
                            {
                                base.SerializeSubElement<T>(subElementName, t, serializationOptions);
                                num--;
                            }
                            num2++;
                        }
                    }
                    long position2 = this.binaryWriter.BaseStream.Position;
                    uint value = (uint)(position2 - position);
                    this.binaryWriter.Seek((int)position, SeekOrigin.Begin);
                    this.binaryWriter.Write(value);
                    this.binaryWriter.Seek((int)position2, SeekOrigin.Begin);
                }
            }
            else
            {
                long position3 = this.binaryReader.BaseStream.Position;
                uint num3 = this.binaryReader.ReadUInt32();
                long offset = position3 + (long)((ulong)num3);
                bool flag = this.SerializeAttribute<bool>("Null", false);
                int num4 = this.SerializeAttribute<int>("Count", 0);
                if (flag)
                {
                    list = null;
                }
                else
                {
                    if (list == null)
                    {
                        list = new List<T>(num4);
                    }
                    if ((serializationOptions & SerializationOption.AppendContent) != SerializationOption.AppendContent)
                    {
                        if ((serializationOptions & SerializationOption.DontInstantiateElements) != SerializationOption.DontInstantiateElements)
                        {
                            list.Clear();
                        }
                    }
                }
                if (list != null)
                {
                    subElementName = (subElementName ?? string.Format("{0}Item", elementName));
                    int j = 0;
                    for (int k = 0; k < num4; k++)
                    {
                        if ((serializationOptions & SerializationOption.DontInstantiateElements) == SerializationOption.DontInstantiateElements)
                        {
                            string text = this.SerializeAttribute<string>("Type", string.Empty);
                            text = ((!string.IsNullOrEmpty(text)) ? text : typeof(T).AssemblyQualifiedName);
                            while (j < list.Count)
                            {
                                if (list[j] != null)
                                {
                                    T t2 = list[j];
                                    if (!(t2.GetType().AssemblyQualifiedName != text))
                                    {
                                        break;
                                    }
                                }
                                j++;
                            }
                            if (j < list.Count)
                            {
                                list[j] = base.SerializeSubElement<T>(subElementName, list[j], serializationOptions);
                            }
                            else
                            {
                                this.binaryReader.BaseStream.Seek(offset, SeekOrigin.Begin);
                            }
                            j++;
                        }
                        else
                        {
                            list.Add(base.SerializeSubElement<T>(subElementName, default(T), serializationOptions));
                        }
                    }
                }
            }
            return list;
        }

        public override Dictionary<TKey, TValue> SerializeElement<TKey, TValue>(string elementName, Dictionary<TKey, TValue> dictionary, SerializationOption serializationOptions)
        {
            if ((serializationOptions & SerializationOption.AppendContent) == SerializationOption.AppendContent && (serializationOptions & SerializationOption.DontInstantiateElements) == SerializationOption.DontInstantiateElements)
            {
                //throw new Diagnostics.Exception("Incompatible serialization options. AppendContent and DontInstantiateElements can't be activated together.");
            }
            if ((serializationOptions & SerializationOption.DontInstantiateElements) == SerializationOption.DontInstantiateElements)
            {
                throw new NotImplementedException();
            }
            Serializer.SerializationMode mode = this.Mode;
            if (mode != Serializer.SerializationMode.Read)
            {
                if (mode == Serializer.SerializationMode.Write)
                {
                    long position = this.binaryWriter.BaseStream.Position;
                    this.binaryWriter.Seek(4, SeekOrigin.Current);
                    if (dictionary == null)
                    {
                        this.SerializeAttribute<bool>("Null", true);
                    }
                    else
                    {
                        this.SerializeAttribute<int>("Count", dictionary.Count);
                        bool flag = base.CanSerializeTypeAsAttribute(typeof(TKey));
                        bool flag2 = flag && base.CanSerializeTypeAsAttribute(typeof(TValue));
                        foreach (KeyValuePair<TKey, TValue> current in dictionary)
                        {
                            if (flag)
                            {
                                this.SerializeAttribute<TKey>("Key", current.Key);
                            }
                            else
                            {
                                base.SerializeSubElement<TKey>("Key", current.Key, serializationOptions);
                            }
                            if (flag2)
                            {
                                this.SerializeAttribute<TValue>("Value", current.Value);
                            }
                            else
                            {
                                base.SerializeSubElement<TValue>("Value", current.Value, serializationOptions);
                            }
                        }
                    }
                    long position2 = this.binaryWriter.BaseStream.Position;
                    uint value = (uint)(position2 - position);
                    this.binaryWriter.Seek((int)position, SeekOrigin.Begin);
                    this.binaryWriter.Write(value);
                    this.binaryWriter.Seek((int)position2, SeekOrigin.Begin);
                }
            }
            else
            {
                long position3 = this.binaryReader.BaseStream.Position;
                uint num = this.binaryReader.ReadUInt32();
                long num2 = position3 + (long)((ulong)num);
                bool flag3 = this.SerializeAttribute<bool>("Null", false);
                int num3 = this.SerializeAttribute<int>("Count", 0);
                if (flag3)
                {
                    dictionary = null;
                }
                else
                {
                    if (dictionary == null)
                    {
                        dictionary = new Dictionary<TKey, TValue>(num3);
                    }
                    if ((serializationOptions & SerializationOption.AppendContent) == SerializationOption.Default)
                    {
                        dictionary.Clear();
                    }
                }
                if (dictionary != null)
                {
                    bool flag4 = base.CanSerializeTypeAsAttribute(typeof(TKey));
                    bool flag5 = flag4 && base.CanSerializeTypeAsAttribute(typeof(TValue));
                    for (int i = 0; i < num3; i++)
                    {
                        TKey tKey = default(TKey);
                        TValue value2 = default(TValue);
                        if (flag4)
                        {
                            tKey = this.SerializeAttribute<TKey>("Key", tKey);
                        }
                        if (flag5)
                        {
                            if (!dictionary.TryGetValue(tKey, out value2))
                            {
                                value2 = default(TValue);
                            }
                            value2 = this.SerializeAttribute<TValue>("Value", value2);
                        }
                        if (!flag4)
                        {
                            tKey = base.SerializeSubElement<TKey>("Key", tKey, serializationOptions);
                        }
                        if (!flag5)
                        {
                            if (!dictionary.TryGetValue(tKey, out value2))
                            {
                                value2 = default(TValue);
                            }
                            value2 = base.SerializeSubElement<TValue>("Value", value2, serializationOptions);
                        }
                        if (dictionary.ContainsKey(tKey))
                        {
                            dictionary[tKey] = value2;
                        }
                        else
                        {
                            dictionary.Add(tKey, value2);
                        }
                    }
                }
            }
            return dictionary;
        }

        public override void Skip(string elementName)
        {
            Serializer.SerializationMode mode = this.Mode;
            if (mode != Serializer.SerializationMode.Read)
            {
                return;
            }
            throw new NotImplementedException();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (this.binaryWriter != null)
                {
                    this.binaryWriter.Flush();
                }
                if (this.binaryReader != null)
                {
                }
            }
            this.binaryWriter = null;
            this.binaryReader = null;
        }
    }
}
