﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Reactive.Serialization
{
    [Flags]
    public enum SerializationOption
    {
        Default = 0,
        SerializeTypeName = 1,
        CData = 2,
        AppendContent = 4,
        DontInstantiateElements = 8
    }

    public abstract class Serializer : IDisposable, IAttributesSerializer, IElementsSerializer
    {
        protected struct MyStruct : ISerializable, IEquatable<Serializer.MyStruct>
        {
            public long LongValue;

            public bool Equals(Serializer.MyStruct other)
            {
                return this.LongValue == other.LongValue;
            }

            public void SerializeAttributes(IAttributesSerializer serializer, IVersionProvider versionProvider)
            {
                this.LongValue = serializer.SerializeAttribute<long>("LongValue", this.LongValue);
            }

            public void SerializeElements(IElementsSerializer serializer, IVersionProvider versionProvider)
            {
            }

            public override string ToString()
            {
                return string.Format("LongValue: {0}", this.LongValue);
            }
        }

        [SerializationVersion(2u)]
        protected class A : ISerializable, IEquatable<Serializer.A>
        {
            public double DoubleValue;

            public DateTime Date;

            public Guid GUID;

            public void SerializeAttributes(IAttributesSerializer serializer, IVersionProvider versionProvider)
            {
            }

            public void SerializeElements(IElementsSerializer serializer, IVersionProvider versionProvider)
            {
                uint versionNumber = versionProvider.GetVersionNumber();
                this.DoubleValue = serializer.SerializeElement<double>("DoubleValue", this.DoubleValue);
                if (versionNumber >= 1u)
                {
                    this.Date = serializer.SerializeElement<DateTime>("Date", this.Date);
                }
                if (versionNumber >= 2u)
                {
                    this.GUID = serializer.SerializeElement<Guid>("GUID", this.GUID);
                }
            }

            public virtual bool Equals(Serializer.A other)
            {
                return this.DoubleValue == other.DoubleValue && this.Date.Equals(other.Date) && this.GUID.Equals(other.GUID);
            }
        }

        [SerializationVersion(1u)]
        protected class B : Serializer.A, ISerializable, IEquatable<Serializer.B>
        {
            public byte ByteValue;

            public sbyte SByteValue;

            public new void SerializeAttributes(IAttributesSerializer serializer, IVersionProvider versionProvider)
            {
            }

            public new void SerializeElements(IElementsSerializer serializer, IVersionProvider versionProvider)
            {
                base.SerializeElements(serializer, versionProvider);
                uint versionNumber = versionProvider.GetVersionNumber();
                this.ByteValue = serializer.SerializeElement<byte>("ByteValue", this.ByteValue);
                if (versionNumber >= 1u)
                {
                    this.SByteValue = serializer.SerializeElement<sbyte>("SByteValue", this.SByteValue);
                }
            }

            public bool Equals(Serializer.B other)
            {
                return base.Equals(other) && this.ByteValue != other.ByteValue && (int)this.SByteValue != (int)other.SByteValue;
            }
        }

        [SerializationVersion(1u)]
        protected class TestData : ISerializable, IEquatable<Serializer.TestData>
        {
            public enum TestEnum
            {
                FirstValue,
                SecondValue
            }

            public bool BoolValue;

            public float FloatValue;

            public string StringValue;

            public uint UintValue;

            public Serializer.TestData.TestEnum EnumValue;

            public Serializer.TestData InternalData;

            public Serializer.MyStruct Struct;

            public List<Serializer.A[]> ListValue = new List<Serializer.A[]>();

            public Dictionary<string, int> DictionaryValue = new Dictionary<string, int>();

            public List<Dictionary<Serializer.TestData.TestEnum, Serializer.A>>[] UglyData;

            void ISerializable.SerializeAttributes(IAttributesSerializer serializer, IVersionProvider versionProvider)
            {
                this.BoolValue = serializer.SerializeAttribute<bool>("BoolValue", this.BoolValue);
            }

            void ISerializable.SerializeElements(IElementsSerializer serializer, IVersionProvider versionProvider)
            {
                if (versionProvider.GetVersionNumber() >= 1u)
                {
                    this.UintValue = serializer.SerializeElement<uint>("UintValue", this.UintValue);
                }
                this.FloatValue = serializer.SerializeElement<float>("FloatValue", this.FloatValue);
                this.StringValue = serializer.SerializeElement<string>("StringValue", this.StringValue);
                this.EnumValue = serializer.SerializeElement<Serializer.TestData.TestEnum>("EnumValue", this.EnumValue);
                this.InternalData = serializer.SerializeElement<Serializer.TestData>("InternalData", this.InternalData);
                this.Struct = serializer.SerializeElement<Serializer.MyStruct>("Struct", this.Struct);
                this.ListValue = serializer.SerializeElement<Serializer.A[]>("ListValue", this.ListValue, "ListElementCustomName", SerializationOption.SerializeTypeName);
                this.DictionaryValue = serializer.SerializeElement<string, int>("DictionaryValue", this.DictionaryValue);
                this.UglyData = serializer.SerializeElement<List<Dictionary<Serializer.TestData.TestEnum, Serializer.A>>>("UglyData", this.UglyData, SerializationOption.SerializeTypeName);
            }

            public override bool Equals(object obj)
            {
                Serializer.TestData testData = obj as Serializer.TestData;
                return testData != null && this.Equals(testData);
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            public bool Equals(Serializer.TestData other)
            {
                if (this.BoolValue != other.BoolValue)
                {
                    return false;
                }
                if (this.FloatValue != other.FloatValue)
                {
                    return false;
                }
                if (this.StringValue != other.StringValue)
                {
                    return false;
                }
                if (this.EnumValue != other.EnumValue)
                {
                    return false;
                }
                if (!this.Struct.Equals(other.Struct))
                {
                    return false;
                }
                if ((this.InternalData != null && other.InternalData == null) || (this.InternalData == null && other.InternalData != null))
                {
                    return false;
                }
                if (this.InternalData != null && other.InternalData != null && !this.InternalData.Equals(other.InternalData))
                {
                    return false;
                }
                if ((this.ListValue != null && other.ListValue == null) || (this.ListValue == null && other.ListValue != null))
                {
                    return false;
                }
                if (this.ListValue != null && other.ListValue != null)
                {
                    if (this.ListValue.Count != other.ListValue.Count)
                    {
                        return false;
                    }
                    for (int i = 0; i < this.ListValue.Count; i++)
                    {
                        Serializer.A[] array = this.ListValue[i];
                        Serializer.A[] array2 = other.ListValue[i];
                        if ((array != null && array2 == null) || (array == null && array2 != null))
                        {
                            return false;
                        }
                        if (array != null && array2 != null)
                        {
                            if (array.Length != array2.Length)
                            {
                                return false;
                            }
                            for (int j = 0; j < array.Length; j++)
                            {
                                if (array[j] != null || array2[j] != null)
                                {
                                    if ((array[j] == null && array2[j] != null) || (array[j] != null && array2[j] == null))
                                    {
                                        return false;
                                    }
                                    if (!array[j].Equals(array2[j]))
                                    {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
                if ((this.DictionaryValue != null && other.DictionaryValue == null) || (this.DictionaryValue == null && other.DictionaryValue != null))
                {
                    return false;
                }
                if (this.DictionaryValue != null && other.DictionaryValue != null)
                {
                    if (this.DictionaryValue.Count != other.DictionaryValue.Count)
                    {
                        return false;
                    }
                    foreach (KeyValuePair<string, int> current in this.DictionaryValue)
                    {
                        int obj;
                        if (!other.DictionaryValue.TryGetValue(current.Key, out obj))
                        {
                            bool result = false;
                            return result;
                        }
                        if (!current.Value.Equals(obj))
                        {
                            bool result = false;
                            return result;
                        }
                    }
                }
                if ((this.UglyData != null && other.UglyData == null) || (this.UglyData == null && other.UglyData != null))
                {
                    return false;
                }
                if (this.UglyData != null && other.UglyData != null)
                {
                    if (this.UglyData.Length != other.UglyData.Length)
                    {
                        return false;
                    }
                    for (int k = 0; k < this.UglyData.Length; k++)
                    {
                        List<Dictionary<Serializer.TestData.TestEnum, Serializer.A>> list = this.UglyData[k];
                        List<Dictionary<Serializer.TestData.TestEnum, Serializer.A>> list2 = other.UglyData[k];
                        if ((list != null && list2 == null) || (list == null && list2 != null))
                        {
                            return false;
                        }
                        if (list != null && list2 != null)
                        {
                            if (list.Count != list2.Count)
                            {
                                return false;
                            }
                            for (int l = 0; l < this.UglyData.Length; l++)
                            {
                                Dictionary<Serializer.TestData.TestEnum, Serializer.A> dictionary = list[l];
                                Dictionary<Serializer.TestData.TestEnum, Serializer.A> dictionary2 = list2[l];
                                if ((dictionary != null && dictionary2 == null) || (dictionary == null && dictionary2 != null))
                                {
                                    return false;
                                }
                                foreach (KeyValuePair<Serializer.TestData.TestEnum, Serializer.A> current2 in dictionary)
                                {
                                    Serializer.A other2;
                                    if (!dictionary2.TryGetValue(current2.Key, out other2))
                                    {
                                        bool result = false;
                                        return result;
                                    }
                                    if (!current2.Value.Equals(other2))
                                    {
                                        bool result = false;
                                        return result;
                                    }
                                }
                            }
                        }
                    }
                }
                return true;
            }
        }

        public enum SerializationMode
        {
            Read,
            Write
        }

        private readonly Type serializationVersionAttributeType = typeof(SerializationVersionAttribute);

        protected Dictionary<Type, VersionProvider> versionProvidersByType = new Dictionary<Type, VersionProvider>();

        private MethodInfo serializeArrayMethodInfo;

        private MethodInfo serializeListMethodInfo;

        private MethodInfo serializeDictionaryMethodInfo;

        public Serializer.SerializationMode Mode
        {
            get;
            private set;
        }

        protected Serializer(Stream stream, Serializer.SerializationMode mode)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }
            this.Mode = mode;
            Type typeFromHandle = typeof(Serializer);
            MethodInfo[] methods = typeFromHandle.GetMethods();
            for (int i = 0; i < methods.Length; i++)
            {
                MethodInfo methodInfo = methods[i];
                ParameterInfo[] parameters = methodInfo.GetParameters();
                if (!(methodInfo.Name != "SerializeElement") && parameters.Length == 3)
                {
                    if (!(parameters[2].Name != "serializationOptions"))
                    {
                        if (methodInfo.ReturnType.IsArray)
                        {
                            this.serializeArrayMethodInfo = methodInfo;
                        }
                        else if (typeof(IList).IsAssignableFrom(methodInfo.ReturnType))
                        {
                            this.serializeListMethodInfo = methodInfo;
                        }
                        else if (typeof(IDictionary).IsAssignableFrom(methodInfo.ReturnType))
                        {
                            this.serializeDictionaryMethodInfo = methodInfo;
                        }
                    }
                }
            }
            if (mode != Serializer.SerializationMode.Read)
            {
                if (mode == Serializer.SerializationMode.Write)
                {
                    if (!stream.CanWrite)
                    {
                        throw new ArgumentException("The stream can't be write.", "stream");
                    }
                }
            }
            else if (!stream.CanRead)
            {
                throw new ArgumentException("The stream can't be read.", "stream");
            }
        }

        protected static Serializer.TestData GetTestDataInstance()
        {
            Serializer.TestData testData = new Serializer.TestData
            {
                BoolValue = true,
                FloatValue = 2.4f,
                UintValue = 112u,
                StringValue = "This is a test of serialization",
                InternalData = new Serializer.TestData
                {
                    FloatValue = 0.333333343f
                },
                Struct = new Serializer.MyStruct
                {
                    LongValue = 203094985L
                }
            };
            testData.ListValue.Add(new Serializer.A[]
            {
                new Serializer.A
                {
                    DoubleValue = 3.1415926535897931,
                    Date = new DateTime(2015, 4, 28, 11, 24, 42),
                    GUID = Guid.NewGuid()
                },
                new Serializer.A
                {
                    DoubleValue = 0.33333333333333331
                },
                null,
                new Serializer.B
                {
                    GUID = Guid.NewGuid(),
                    DoubleValue = 2.3,
                    ByteValue = 42,
                    SByteValue = 3
                }
            });
            testData.ListValue.Add(null);
            testData.ListValue.Add(new Serializer.A[0]);
            testData.DictionaryValue.Add("SomeKey", 3);
            testData.DictionaryValue.Add("Answer", 42);
            testData.DictionaryValue.Add("This is a very long key", 6548);
            testData.UglyData = new List<Dictionary<Serializer.TestData.TestEnum, Serializer.A>>[1];
            testData.UglyData[0] = new List<Dictionary<Serializer.TestData.TestEnum, Serializer.A>>(1);
            Dictionary<Serializer.TestData.TestEnum, Serializer.A> dictionary = new Dictionary<Serializer.TestData.TestEnum, Serializer.A>();
            testData.UglyData[0].Add(dictionary);
            dictionary.Add(Serializer.TestData.TestEnum.FirstValue, new Serializer.A());
            dictionary.Add(Serializer.TestData.TestEnum.SecondValue, new Serializer.B());
            return testData;
        }

        ~Serializer()
        {
            this.Dispose(false);
            GC.SuppressFinalize(this);
        }

        public void Dispose()
        {
            this.Dispose(true);
        }

        public abstract T SerializeAttribute<T>(string attributeName, T value);

        public T SerializeElement<T>(string elementName, T value)
        {
            return this.SerializeElement<T>(elementName, value, SerializationOption.Default);
        }

        public abstract T SerializeElement<T>(string elementName, T value, SerializationOption serializationOptions);

        public T[] SerializeElement<T>(string elementName, T[] array)
        {
            return this.SerializeElement<T>(elementName, array, null, SerializationOption.Default);
        }

        public T[] SerializeElement<T>(string elementName, T[] array, SerializationOption serializationOptions)
        {
            return this.SerializeElement<T>(elementName, array, null, serializationOptions);
        }

        public T[] SerializeElement<T>(string elementName, T[] array, string subElementName)
        {
            return this.SerializeElement<T>(elementName, array, subElementName, SerializationOption.Default);
        }

        public abstract T[] SerializeElement<T>(string elementName, T[] array, string subElementName, SerializationOption serializationOptions);

        public List<T> SerializeElement<T>(string elementName, List<T> list)
        {
            return this.SerializeElement<T>(elementName, list, null, SerializationOption.Default);
        }

        public List<T> SerializeElement<T>(string elementName, List<T> list, SerializationOption serializationOptions)
        {
            return this.SerializeElement<T>(elementName, list, null, serializationOptions);
        }

        public List<T> SerializeElement<T>(string elementName, List<T> list, string subElementName)
        {
            return this.SerializeElement<T>(elementName, list, subElementName, SerializationOption.Default);
        }

        public abstract List<T> SerializeElement<T>(string elementName, List<T> list, string subElementName, SerializationOption serializationOptions);

        public Dictionary<TKey, TValue> SerializeElement<TKey, TValue>(string elementName, Dictionary<TKey, TValue> dictionary)
        {
            return this.SerializeElement<TKey, TValue>(elementName, dictionary, SerializationOption.Default);
        }

        public abstract Dictionary<TKey, TValue> SerializeElement<TKey, TValue>(string elementName, Dictionary<TKey, TValue> dictionary, SerializationOption serializationOptions);

        public abstract void Skip(string elementName);

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.versionProvidersByType.Clear();
            }
            this.serializeArrayMethodInfo = null;
            this.serializeListMethodInfo = null;
            this.serializeDictionaryMethodInfo = null;
        }

        protected VersionProvider GetVersionProvider<T>(T value) where T : ISerializable
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            Type type = value.GetType();
            VersionProvider versionProvider;
            if (this.versionProvidersByType.TryGetValue(type, out versionProvider))
            {
                return versionProvider;
            }
            versionProvider = new VersionProvider();
            versionProvider.InitializeFromType(type);
            this.versionProvidersByType.Add(type, versionProvider);
            return versionProvider;
        }

        protected T SerializeSubElement<T>(string elementName, T value, SerializationOption serializationOptions)
        {
            Type typeFromHandle = typeof(T);
            if (typeFromHandle.IsArray)
            {
                Type elementType = typeFromHandle.GetElementType();
                MethodInfo methodInfo = this.serializeArrayMethodInfo.MakeGenericMethod(new Type[]
                {
                    elementType
                });
                return (T)((object)methodInfo.Invoke(this, new object[]
                {
                    elementName,
                    value,
                    serializationOptions
                }));
            }
            if (typeof(IList).IsAssignableFrom(typeFromHandle))
            {
                Type[] genericArguments = typeFromHandle.GetGenericArguments();
                Type type = genericArguments[0];
                MethodInfo methodInfo2 = this.serializeListMethodInfo.MakeGenericMethod(new Type[]
                {
                    type
                });
                return (T)((object)methodInfo2.Invoke(this, new object[]
                {
                    elementName,
                    value,
                    serializationOptions
                }));
            }
            if (typeof(IDictionary).IsAssignableFrom(typeFromHandle))
            {
                Type[] genericArguments2 = typeFromHandle.GetGenericArguments();
                Type type2 = genericArguments2[0];
                Type type3 = genericArguments2[1];
                MethodInfo methodInfo3 = this.serializeDictionaryMethodInfo.MakeGenericMethod(new Type[]
                {
                    type2,
                    type3
                });
                return (T)((object)methodInfo3.Invoke(this, new object[]
                {
                    elementName,
                    value,
                    serializationOptions
                }));
            }
            return this.SerializeElement<T>(elementName, value, serializationOptions);
        }

        protected bool CanSerializeTypeAsAttribute(Type type)
        {
            return !type.IsArray && !typeof(IList).IsAssignableFrom(type) && !typeof(IDictionary).IsAssignableFrom(type) && !typeof(ISerializable).IsAssignableFrom(type);
        }

        protected virtual string GetVersionAttributeNameFromVersionID(string versionID)
        {
            if (string.IsNullOrEmpty(versionID))
            {
                return "Version";
            }
            return string.Format("{0}.Version", versionID);
        }

        protected void ReleaseVersionProvider(VersionProvider versionProvider)
        {
        }

        protected VersionProvider SerializeVersionAttribute(VersionProvider versionProvider)
        {
            if (versionProvider == null)
            {
                throw new ArgumentNullException("versionProvider");
            }
            foreach (SerializationVersion current in ((IEnumerable<SerializationVersion>)versionProvider))
            {
                if (current.Number > 0u)
                {
                    string versionAttributeNameFromVersionID = this.GetVersionAttributeNameFromVersionID(current.ID);
                    uint number = this.SerializeAttribute<uint>(versionAttributeNameFromVersionID, current.Number);
                    versionProvider.RegisterVersion(new SerializationVersion(number, current.ID));
                }
            }
            return versionProvider;
        }

        protected string ToString<T>(T value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            if (value is float)
            {
                return Convert.ToSingle(value).ToString("R");
            }
            if (value is double)
            {
                return Convert.ToDouble(value).ToString("R");
            }
            if (value is Guid)
            {
                return value.ToString();
            }
            if (value is Type)
            {
                return (value as Type).AssemblyQualifiedName;
            }
            if (value is IStringConvertible<T>)
            {
                return ((IStringConvertible<T>)((object)value)).ConvertToString();
            }
            return (string)Convert.ChangeType(value, TypeCode.String);
        }

        protected T FromString<T>(string stringValue)
        {
            T result = default(T);
            if (stringValue != null)
            {
                try
                {
                    Type typeFromHandle = typeof(T);
                    if (!string.IsNullOrEmpty(stringValue))
                    {
                        if (typeFromHandle.IsEnum)
                        {
                            T result2 = (T)((object)Enum.Parse(typeFromHandle, stringValue));
                            return result2;
                        }
                        if (typeFromHandle == typeof(Guid))
                        {
                            object obj = new Guid(stringValue);
                            T result2 = (T)((object)obj);
                            return result2;
                        }
                        if (typeFromHandle == typeof(Type))
                        {
                            object type = Type.GetType(stringValue);
                            T result2 = (T)((object)type);
                            return result2;
                        }
                    }
                    if (typeof(IStringConvertible<T>).IsAssignableFrom(typeFromHandle))
                    {
                        IStringConvertible<T> stringConvertible = (IStringConvertible<T>)FormatterServices.GetUninitializedObject(typeFromHandle);
                        T result2 = stringConvertible.ConvertFromString(stringValue);
                        return result2;
                    }
                    if (typeFromHandle == typeof(string) || !string.IsNullOrEmpty(stringValue))
                    {
                        result = (T)((object)Convert.ChangeType(stringValue, typeFromHandle));
                    }
                }
                catch
                {
                }
                return result;
            }
            if (typeof(T) == typeof(string))
            {
                result = (T)((object)string.Empty);
            }
            return result;
        }
    }
}
