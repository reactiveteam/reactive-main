﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reactive.Serialization
{
    public interface IAttributesSerializer
    {
        Serializer.SerializationMode Mode
        {
            get;
        }

        T SerializeAttribute<T>(string attributeName, T value);
    }

    public interface IElementsSerializer
    {
        Serializer.SerializationMode Mode
        {
            get;
        }

        T SerializeElement<T>(string elementName, T value);

        T SerializeElement<T>(string elementName, T value, SerializationOption serializationOptions);

        T[] SerializeElement<T>(string elementName, T[] array);

        T[] SerializeElement<T>(string elementName, T[] array, SerializationOption serializationOptions);

        T[] SerializeElement<T>(string elementName, T[] array, string subElementName);

        T[] SerializeElement<T>(string elementName, T[] array, string subElementName, SerializationOption serializationOptions);

        List<T> SerializeElement<T>(string elementName, List<T> list);

        List<T> SerializeElement<T>(string elementName, List<T> list, SerializationOption serializationOptions);

        List<T> SerializeElement<T>(string elementName, List<T> list, string subElementName);

        List<T> SerializeElement<T>(string elementName, List<T> list, string subElementName, SerializationOption serializationOptions);

        Dictionary<TKey, TValue> SerializeElement<TKey, TValue>(string elementName, Dictionary<TKey, TValue> dictionary);

        Dictionary<TKey, TValue> SerializeElement<TKey, TValue>(string elementName, Dictionary<TKey, TValue> dictionary, SerializationOption serializationOptions);

        void Skip(string elementName);
    }

    public interface ISerializable
    {
        void SerializeAttributes(IAttributesSerializer serializer, IVersionProvider versionProvider);

        void SerializeElements(IElementsSerializer serializer, IVersionProvider versionProvider);
    }

    public interface ISerializationFilter
    {
        bool SkipSerialization
        {
            get;
        }
    }

    public interface IStringConvertible<out T>
    {
        string ConvertToString();

        T ConvertFromString(string value);
    }

    public interface IVersionProvider
    {
        uint GetVersionNumber();
    }
}
