﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

namespace Reactive.Serialization
{
    public class VersionProvider : IEnumerable, IEnumerable<SerializationVersion>, IVersionProvider
    {
        private static readonly Type SerializationVersionAttributeType = typeof(SerializationVersionAttribute);

        private static readonly Type SerializableInterfaceType = typeof(ISerializable);

        private List<SerializationVersion> versions = new List<SerializationVersion>();

        IEnumerator<SerializationVersion> IEnumerable<SerializationVersion>.GetEnumerator()
        {
            return this.versions.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.versions.GetEnumerator();
        }

        public uint GetVersionNumber()
        {
            if (this.versions.Count == 0)
            {
                return 0u;
            }
            StackFrame stackFrame = new StackFrame(1);
            MethodBase method = stackFrame.GetMethod();
            string fullName = method.DeclaringType.FullName;
            for (int i = 0; i < this.versions.Count; i++)
            {
                if (this.versions[i].ID == fullName)
                {
                    return this.versions[i].Number;
                }
            }
            return 0u;
        }

        internal uint GetVersionNumber(string versionID)
        {
            if (this.versions.Count == 0)
            {
                return 0u;
            }
            for (int i = 0; i < this.versions.Count; i++)
            {
                if (this.versions[i].ID == versionID)
                {
                    return this.versions[i].Number;
                }
            }
            return 0u;
        }

        internal void InitializeFromType(Type type)
        {
            this.Clear();
            while (type != null && VersionProvider.SerializableInterfaceType.IsAssignableFrom(type))
            {
                object[] customAttributes = type.GetCustomAttributes(VersionProvider.SerializationVersionAttributeType, false);
                SerializationVersionAttribute serializationVersionAttribute = (customAttributes.Length <= 0) ? null : (customAttributes[0] as SerializationVersionAttribute);
                if (serializationVersionAttribute != null)
                {
                    this.RegisterVersion(new SerializationVersion(serializationVersionAttribute.Version, type.FullName));
                }
                type = type.BaseType;
            }
        }

        internal void RegisterVersion(SerializationVersion version)
        {
            version = new SerializationVersion(version.Number, version.ID ?? string.Empty);
            if (version.ID == null)
            {
                throw new ArgumentNullException("version");
            }
            for (int i = 0; i < this.versions.Count; i++)
            {
                if (this.versions[i].ID == version.ID)
                {
                    uint number = this.versions[i].Number;
                    this.versions[i] = version;
                    return;
                }
            }
            this.versions.Add(version);
        }

        private void Clear()
        {
            this.versions.Clear();
        }
    }


    internal struct SerializationVersion
    {
        public uint Number
        {
            get;
            private set;
        }

        public string ID
        {
            get;
            private set;
        }

        public SerializationVersion(uint number, string id)
        {
            this.Number = number;
            this.ID = id;
        }
    }
}
