﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reactive.Serialization
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false)]
    public class SerializationVersionAttribute : Attribute
    {
        public uint Version
        {
            get;
            private set;
        }

        public SerializationVersionAttribute(uint version)
        {
            this.Version = version;
        }
    }
}
